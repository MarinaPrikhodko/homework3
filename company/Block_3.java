package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Block_3 {
//    public static void main(String[] args) {
//        String[]words=new String[]{"пятка", "чай", "идентификатор", "розетка", "книга"};
//        replaceCharsTo$(words);
//        //System.out.println(Arrays.toString(words));
//    }

    public int getLengthOfShortestWord(String str) {

        String[] splitStr = str.split(" ");
        int min = splitStr[0].length();
        for (int i = 0; i < splitStr.length; i++) {
            if (splitStr[i].length() < min) {
                min = splitStr[i].length();
            }
        }
        return min;
    }

    public  String replaceCharsTo$(String[] words) {

        for (int i = 0; i < words.length; i++) {
            if(words[i].length()>3){
                StringBuilder sb=new StringBuilder(words[i]);
                sb.delete(sb.length()-3,sb.length()).append("$$$");
                words[i]=sb.toString();
               // System.out.println(sb);

//                words[i]=words[i].replace(words[i].charAt(words[i].length()-1),'$')
//                        .replace(words[i].charAt(words[i].length()-2),'$')
//                        .replace(words[i].charAt(words[i].length()-3),'$');
            }
        }
        return Arrays.toString(words);
    }

    public  String printSpaceAfterPunctuationMark(String str){

            char[]ch=new char[]{',',':',';','-','!','?','.'};
            StringBuilder sb=new StringBuilder(str);
            for(int i=0; i<sb.length()-1;i++){
                for(int j=0;j<ch.length;j++){
                    if((sb.charAt(i))==ch[j]&&(sb.charAt(i+1))!=' '){
                        sb.insert(i+1," ");
                    }
                }
            }
        return sb.toString();
    }

    public String saveOnlyOneRepeatingSimbol(String str){

        StringBuilder sb=new StringBuilder(str);
        for(int i=0;i<sb.length();i++){
            for (int j=i+1;j<sb.length();++j) {
                if((sb.charAt(i))==((sb.charAt(j)))){
                    sb.deleteCharAt(j);
                }
            }
        }
        return sb.toString();
    }

    public int getWordCount(String sentence){
       // Scanner sc=new Scanner(System.in);
       // String sentence=sc.nextLine();
        String[]words=sentence.split(" ");
       return words.length;

    }

    public String deletePartFromString(String str){

            StringBuilder sb=new StringBuilder(str);
            sb.delete(sb.length()/2,(sb.length()/2)+10);
            return sb.toString();



    }

    public String reversSpring(String str){

            StringBuilder sb=new StringBuilder(str);
             sb.reverse();
             return sb.toString();
    }

    public String deleteLastWord(String str){

            String[]words=str.split(" ");
            words[words.length-1]="";
            StringBuilder sb=new StringBuilder();
            for(int i=0; i<words.length;i++){
                sb.append(words[i]+" ");
            }
        return sb.toString();
        }


}


