package com.company;

public class Block_2 {

    public String fromIntToString(int number){
        return Integer.toString(number);
    }
    public  String fromDoubleToString(double number){
        return Double.toString(number);
    }
    public int fromStringToInt(String number){
       return Integer.parseInt(number);
    }
    public double fromStringToDouble(String number){
        return Double.parseDouble(number);
    }

    
}
