package com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Scanner;

public class Test_Block_3 {
    static Block_3 b3;
    @BeforeAll
    public static void init(){
        b3=new Block_3();
    }
    @Test
    public void test_getLengthOfShortestWord_2(){
        String str = "предложение, состоящее из разных слов, знаков, букв.";
        int act=2;
        int exp=b3.getLengthOfShortestWord(str);
        Assertions.assertEquals(act,exp);

    }
    @Test
    public void test_getLengthOfShortestWord_3(){
        String str = "это предложение содержит слова";
        int act=3;
        int exp=b3.getLengthOfShortestWord(str);
        Assertions.assertEquals(act,exp);

    }
    @Test
    public void test_getLengthOfShortestWord_1(){
        String str = "набор различных словестных сочетаний";
        int act=5;
        int exp=b3.getLengthOfShortestWord(str);
        Assertions.assertEquals(act,exp);

    }
    @Test
    public void test_replaceCharsTo$_1(){
        String[]words=new String[]{"ноль", "нож", "здание", "цветок", "фармокология", "два","мозгодробилка","кинотеатр","9452код"};
        String act="[н$$$, нож, зда$$$, цве$$$, фармоколо$$$, два, мозгодроби$$$, киноте$$$, 9452$$$]";
        String exp=b3.replaceCharsTo$(words);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void test_replaceCharsTo$_2(){
        String[]words=new String[]{"пятка", "чай", "идентификатор", "розетка", "книга"};
        String act="[пя$$$, чай, идентифика$$$, розе$$$, кн$$$]";
        String exp=b3.replaceCharsTo$(words);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void test_replaceCharsTo$_3(){
        String[]words=new String[]{"вольтметр", "игрушки", "ель", "мозги", "словосочетание"};
        String act="[вольтм$$$, игру$$$, ель, мо$$$, словосочета$$$]";
        String exp=b3.replaceCharsTo$(words);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void test_printSpaceAfterPunctuationMark_1(){
        String str="Строка,в которой после:знаков препинания,не стоят;пробелы! А, надо бы - их поставить?";
        String act="Строка, в которой после: знаков препинания, не стоят; пробелы! А, надо бы - их поставить?";
        String exp=b3.printSpaceAfterPunctuationMark(str);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void test_printSpaceAfterPunctuationMark_2(){
        String str="Точка,точка,запятая,знак тире -и восклицательный!знак.";
        String act="Точка, точка, запятая, знак тире - и восклицательный! знак.";
        String exp=b3.printSpaceAfterPunctuationMark(str);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void test_printSpaceAfterPunctuationMark_3(){
        String str="всякие:разные;буссмысленные- наборы?слов!";
        String act="всякие: разные; буссмысленные- наборы? слов!";
        String exp=b3.printSpaceAfterPunctuationMark(str);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void test_getWordCount_1(){
        String sentence="предложение из пяти слов";
        int act=4;
        int exp=b3.getWordCount(sentence);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void test_getWordCount_2(){
        String sentence="предложение, в котором куча всяких разных слов";
        int act=7;
        int exp=b3.getWordCount(sentence);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void test_getWordCount_3(){
        String sentence="предложение из пяти слов, в которое еще что-то добавили";
        int act=9;
        int exp=b3.getWordCount(sentence);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void test_deletePartFromString_1(){
       String str="Строка, в которой слова разделены пробелами и знаками препинания.";
       String act="Строка, в которой слова разделени и знаками препинания.";
       String exp=b3.deletePartFromString(str);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void test_deletePartFromString_2(){
        String str="предложение из пяти слов";
        String act="предложение ов";
        String exp=b3.deletePartFromString(str);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void test_deletePartFromString_3(){
        String str="это предложение содержит слова";
        String act="это предложениеслова";
        String exp=b3.deletePartFromString(str);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void test_reversSpring_1(){
        String str="предложение из пяти слов";
        String act="волс итяп зи еинежолдерп";
        String exp=b3.reversSpring(str);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void test_reversSpring_2(){
        String str="это предложение содержит слова";
        String act="аволс тижредос еинежолдерп отэ";
        String exp=b3.reversSpring(str);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void test_reversSpring_3(){
        String str="три разных слова";
        String act="аволс хынзар ирт";
        String exp=b3.reversSpring(str);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void test_deleteLastWord_1(){
        String str="предложение из пяти слов";
        String act="предложение из пяти  ";
        String exp=b3.deleteLastWord(str);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void test_deleteLastWord_2(){
        String str="это предложение содержит слова";
        String act="это предложение содержит  ";
        String exp=b3.deleteLastWord(str);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void test_deleteLastWord_3(){
        String str="три разных слова";
        String act="три разных  ";
        String exp=b3.deleteLastWord(str);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void  test_saveOnlyOneRepeatingSimbol_1(){
        String str="три разных слова";
        String act="три азныхслов";
        String exp=b3.saveOnlyOneRepeatingSimbol(str);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void  test_saveOnlyOneRepeatingSimbol_2(){
        String str="предложение из слов";
        String act="предложни зсв";
        String exp=b3.saveOnlyOneRepeatingSimbol(str);
        Assertions.assertEquals(act,exp);
    }
    @Test
    public void  test_saveOnlyOneRepeatingSimbol_3(){
        String str="какой то, набор выражений";
        String act="каой т,нбрвыжеи";
        String exp=b3.saveOnlyOneRepeatingSimbol(str);
        Assertions.assertEquals(act,exp);
    }










}


