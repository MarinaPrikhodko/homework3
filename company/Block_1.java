package com.company;

import java.util.Arrays;

public class Block_1 {

    public static void main(String[] args) {

        System.out.println(lettersFromAtoZ());
        System.out.println(lettersFromAToZReversLowerCase());
        System.out.println(lettersFromAtoЯ());
        System.out.println(digitsFrom_0_To_9());
        System.out.println(showPrintedRangeAscii());


    }
    public static String lettersFromAtoZ(){
        StringBuilder sb=new StringBuilder();
        for(int i=122;i>=97;i--){
            sb.insert(0,(char)i+" ");
        }return sb.toString().toUpperCase();
    }
    public static StringBuilder lettersFromAToZReversLowerCase(){
        StringBuilder sb=new StringBuilder();
        for(int i=122;i>=97;i--){
            sb.insert(0,(char)i+" ");
        }return sb.reverse().deleteCharAt(0);
    }
    public static StringBuilder lettersFromAtoЯ(){
        StringBuilder sb=new StringBuilder();
        for(int i=1103;i>=1072;i--){
            sb.insert(0,(char)i+" ");
        }return sb;
    }

    public static StringBuilder digitsFrom_0_To_9(){
        StringBuilder sb=new StringBuilder();
        for(int i=9; i>=0;i--){
            sb.insert(0,i+" ");
        } return sb;
    }
    public static StringBuilder showPrintedRangeAscii(){
        StringBuilder engUpper=new StringBuilder();
            for(int i=90;i>=65;i--){
                engUpper.insert(0,(char)i+" ");
            }
        StringBuilder engLow=new StringBuilder();
        for(int i=122;i>=97;i--){
            engLow.insert(0,(char)i+" ");
        }
        StringBuilder kirilLow=new StringBuilder();
        for(int i=1103;i>=1072;i--){
            kirilLow.insert(0,(char)i+" ");
        }
        StringBuilder kirilUpper=new StringBuilder();
        for(int i=1071;i>=1040;i--){
            kirilUpper.insert(0,(char)i+" ");
        }
        return engLow.append(engUpper).append(kirilLow).append(kirilUpper);
        

    }
       
    }

