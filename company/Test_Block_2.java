package com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class Test_Block_2 {
    static Block_2 b2;
    @BeforeAll
    public static void init(){
        b2=new Block_2();
    }
    @Test
    public  void test_fromIntToString_5(){
        String exp="5";
        String act=b2.fromIntToString(5);
        Assertions.assertEquals(exp,act);
    }
    @Test
    public  void test_fromIntToString_126(){
        String exp="126";
        String act=b2.fromIntToString(126);
        Assertions.assertEquals(exp,act);
    }
    @Test
    public  void test_fromIntToString_98(){
        String exp="98";
        String act=b2.fromIntToString(98);
        Assertions.assertEquals(exp,act);
    }
    @Test
    public  void test_fromDoubleToString_1_1(){
        String exp="1.1";
        String act=b2.fromDoubleToString(1.1);
        Assertions.assertEquals(exp,act);
    }
    @Test
    public  void test_fromDoubleToString_0_236541125(){
        String exp="0.236541125";
        String act=b2.fromDoubleToString(0.236541125);
        Assertions.assertEquals(exp,act);
    }
    @Test
    public  void test_fromDoubleToString_293_362547(){
        String exp="293.362547";
        String act=b2.fromDoubleToString(293.362547);
        Assertions.assertEquals(exp,act);
    }
    @Test
    public  void test_fromStringToInt_125(){
        int exp=125;
        int act=b2.fromStringToInt("125");
        Assertions.assertEquals(exp,act);
    }
    @Test
    public  void test_fromStringToInt_95874(){
        int exp=95874;
        int act=b2.fromStringToInt("95874");
        Assertions.assertEquals(exp,act);
    }
    @Test
    public  void test_fromStringToDouble_6_3265(){
        double exp=6.3265;
        double act=b2.fromStringToDouble("6.3265");
        Assertions.assertEquals(exp,act);
    }
    @Test
    public  void test_fromStringToDouble_0_32(){
        double exp=0.32;
        double act=b2.fromStringToDouble("0.32");
        Assertions.assertEquals(exp,act);
    }
    @Test
    public  void test_fromStringToDouble_1253_3662(){
        double exp=1253.3662;
        double act=b2.fromStringToDouble("1253.3662");
        Assertions.assertEquals(exp,act);
    }





}
